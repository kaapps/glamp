#include <stdlib.h>
#include <stdio.h>
#include <libgen.h>

#include "h/header.h"
#include "h/gui.h"

void apache_restart() {
	bool res=exec_cmd("service apache2 restart");
	if(res) {
		gui_show_widget("success");
	}
}

void apache_log_file() {
	exec_cmd("gedit /var/log/apache2/error.log");
}

void apache_edit_host(string host_name) {
	char cmd[CHAR_SIZE]="gedit /etc/apache2/sites-available/";
	strcat(cmd, host_name);
	exec_cmd(cmd);
}

bool apache_activate_host(string host_name) {
	char cmd[CHAR_SIZE]="a2ensite ";
	strcat(cmd, host_name);
	return exec_cmd(cmd);
}

bool apache_deactivate_host(string host_name) {
	char cmd[CHAR_SIZE]="a2dissite ";
	strcat(cmd, host_name);
	return exec_cmd(cmd);
}

void apache_delete_host(string host_name) {
	bool res=apache_deactivate_host(host_name);
	if(!res) {
		return;
	}

	char folder[CHAR_SIZE]="/etc/apache2/sites-available/";
	string file_name=strcat(folder, host_name);

	res=file_delete(file_name);
	if(!res) {
		return;
	}

	res=file_delete_lines(host_name, "/etc/hosts");
	if(!res) {
		return;
	}

	apache_restart();
}

void apache_create_host(string host_path) {
	string host_name=basename(host_path);

	char folder[CHAR_SIZE]="/etc/apache2/sites-available/";
	string file_name=strcat(folder, host_name);

	char txt[CHAR_SIZE];

	string host=""
		"<VirtualHost *:80>\n"
		"	ServerName %s\n"
		"	ServerAlias www.%s\n"
		"	DocumentRoot %s\n"
		"	Alias /_pma /usr/share/phpmyadmin/\n"
		"	<Directory %s>\n"
		"		AllowOverride All\n"
		"	</Directory>\n"
		"</VirtualHost>\n";

	sprintf(txt, host, host_name, host_name, host_path, host_path);

	bool res=files_write(file_name, txt);
	if(!res) {
		return;
	}

	char txt2[CHAR_SIZE];
	sprintf(txt2, "echo '127.0.0.1    %s    www.%s' >> /etc/hosts", host_name, host_name);

	res=exec_cmd(txt2);
	if(!res) {
		return;
	}

	res=apache_activate_host(host_name);
	if(!res) {
		return;
	}

	apache_restart();
}
