#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <pwd.h>

#include "h/header.h"

#define TMP_FILE config_files("/tmp_file")

char files_read_res[CHAR_SIZE];

string files_get_home_folder() {
	struct passwd *pw = getpwnam(getlogin() );
	return pw->pw_dir;
}

void files_create_folder_if_not_exists(string folder_name) {
	DIR	*dir = opendir(folder_name);

	if(dir) {
		closedir(dir);
	}
	else {
		mkdir(folder_name, S_IRWXU);
	}
}

bool files_write(string file_name, string txt) {

	FILE *fp;
	fp = fopen(TMP_FILE, "w+");

	if(fp == NULL) {
		return false;
	}

	fprintf(fp, "%s", txt);
	fclose(fp);

	char res[CHAR_SIZE];
	sprintf(res, "cp '%s' '%s'", TMP_FILE, file_name);

	exec_cmd(res);

	return true;
}

string files_read(string file_name) {

	FILE *fp;

	fp = fopen(file_name, "r");

	if(fp == NULL) {
		return NULL;
	}

	fscanf(fp, "%s", files_read_res);
	fclose(fp);

	return files_read_res;
}

struct s_files_list files_list(string folder_name) {
	DIR *dfd;
	struct dirent *dp;
	struct s_files_list res;

	dfd=opendir(folder_name);

	res.count=0;
	while( (dp=readdir(dfd)) != NULL) {
		if(strcmp(dp->d_name, "..")!=0 && strcmp(dp->d_name, ".")!=0) {
			strcpy(res.files[res.count], dp->d_name);
			res.count++;
		}
	}

	closedir(dfd);

	return res;
}

bool file_delete(string name) {
	char txt[CHAR_SIZE]="rm ";
	strcat(txt, name);
	return exec_cmd(txt);
}

bool file_delete_lines(string str, string file_name) {
	char txt[CHAR_SIZE];
	sprintf(txt, "sed -i '/%s/d' '%s'", str, file_name);
	return exec_cmd(txt);
}

