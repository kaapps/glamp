#include <stdio.h>

#include "h/header.h"
#include "h/gui.h"

typedef struct _Private Private;
static struct _Private {
	/* ANJUTA: Widgets declaration for glamp.ui - DO NOT REMOVE */
};

static struct Private* priv = NULL;

/* For testing propose use the local (not installed) ui file */
#define UI_FILE PACKAGE_DATA_DIR"/ui/glamp.ui"
//#define UI_FILE "src/glamp.ui"
#define TOP_WINDOW "window"

GtkBuilder *builder;

void gui_file_chooser_activated() {
	GtkFileChooser * chooser = GTK_FILE_CHOOSER(gui_get_widget("chooser") );
	gtk_file_chooser_set_current_folder(chooser, files_get_home_folder() );
}

void gui_delete_host(GtkTreeView *tree) {
	GtkTreeSelection	*selection;
	GtkTreeModel		*model;
	GtkTreeIter			iter;

	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(tree));

	if(gtk_tree_selection_get_selected(selection, &model, &iter)) {
		gchar *name;
		gtk_tree_model_get (model, &iter, 0, &name, -1);
		apache_delete_host(name);
		g_free(name);
	}
}

bool str_postfix(string str, string postfix) {
	int slen = strlen(str);
	int plen = strlen(postfix);

	bool res=(slen >= plen) && (0 == strcmp(str + slen - plen, postfix));

	return res;
}

void gui_get_hosts_list(GtkListStore *list) {

	gtk_list_store_clear(list);

	GValue value = G_VALUE_INIT;
	g_value_init(&value, G_TYPE_STRING);
	GtkTreeIter iter;

	struct s_files_list fid=files_list("/etc/apache2/sites-available/");

	for(int i=0; i<fid.count; i++) {
		if(str_postfix(fid.files[i], "~") ) {
			continue;
		}

		g_value_set_string(&value, fid.files[i]);
		gtk_list_store_insert(list, &iter, 0);
		gtk_list_store_set_value(list, &iter, 0, &value);
	}
}

GtkWidget * gui_get_widget(string name) {
	return GTK_WIDGET(gtk_builder_get_object(builder, name) );
}

void gui_show_widget(string name) {
	GtkWidget * widget=gui_get_widget(name);
	gtk_widget_show(widget);
}

void gui_hide_widget(string name) {
	GtkWidget * widget=gui_get_widget(name);
	gtk_widget_hide(widget);
}

void gui_hide_about_dialog() {
	gui_hide_widget("about");
}

void gui_destroy(GtkWidget * widget, gpointer data) {
	gtk_main_quit();
}

void destroy(GtkWidget * widget, gpointer data) {
	gtk_main_quit();
}


void gui_init_folder(GtkFileChooser *chooser) {
	gchar *folder_name=gtk_file_chooser_get_filename(chooser);
	apache_create_host(folder_name);
}

void gui_edit_host(GtkTreeView *tree) {
	GtkTreeSelection	*selection;
	GtkTreeModel		*model;
	GtkTreeIter			iter;

	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(tree) );

	if(gtk_tree_selection_get_selected(selection, &model, &iter) ) {
		gchar *name;
		gtk_tree_model_get(model, &iter, 0, &name, -1);
		apache_edit_host(name);
		g_free(name);
	}
}

static GtkWidget* gui_create_window() {
	GtkWidget *window;

	GError* error = NULL;

	/* Load UI from file */
	builder = gtk_builder_new();
	if (!gtk_builder_add_from_file(builder, UI_FILE, &error) ) {
		g_critical("Couldn't load builder file: %s", error->message);
		g_error_free(error);
	}

	/* Auto-connect signal handlers */
	gtk_builder_connect_signals(builder, NULL);

	/* Get the window object from the ui file */
	window = GTK_WIDGET(gtk_builder_get_object(builder, TOP_WINDOW));
	if (!window) {
		g_critical(
			"Widget \"%s\" is missing in file %s.",
			TOP_WINDOW,
			UI_FILE
		);
	}

	priv = g_malloc(sizeof (struct _Private));
	/* ANJUTA: Widgets initialization for glamp.ui - DO NOT REMOVE */

	return window;
}

void gui_main(int *argc, char ***argv) {
	GtkWidget *window;

	#ifdef ENABLE_NLS
		bindtextdomain(GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
		bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
		textdomain(GETTEXT_PACKAGE);
	#endif

	gtk_init(argc, argv);

	window = gui_create_window();
	gtk_widget_show(window);

	gtk_main();

	g_free(priv);
}
