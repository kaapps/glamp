#include "h/header.h"
#include "h/gui.h"

void install() {
	bool res=exec_cmd("xterm -e \"aptitude install --assume-yes apache2\"");
	if(!res) {
		return;
	}

	res=exec_cmd("a2enmod rewrite");
	if(!res) {
		return;
	}

	res=exec_cmd("xterm -e \"aptitude install --assume-yes php5 php5-gd php5-mysql php5-imagick\"");
	if(!res) {
		return;
	}

	res=exec_cmd("xterm -e \"aptitude install --assume-yes mysql-client-5.5 mysql-server-5.5\"");
	if(!res) {
		return;
	}

	res=exec_cmd("xterm -e \"aptitude install --assume-yes phpmyadmin\"");
	if(!res) {
		return;
	}

	gui_show_widget("success");
}
