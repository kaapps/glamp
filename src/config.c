#include "h/header.h"

string config_user_config_folder() {
	string dir=files_get_home_folder();
	return strcat(dir, "/.config");
}

string config_folder() {
	string dir=config_user_config_folder();
	return strcat(dir, "/glamp");
}

string config_files(string file_name) {
	string folder=config_folder();
	return strcat(folder, file_name);
}

void config_check_dirs() {
	files_create_folder_if_not_exists(config_user_config_folder() );
	files_create_folder_if_not_exists(config_folder() );
}

