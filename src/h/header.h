#ifndef _GLAMP_H
#define _GLAMP_H

enum bool {
	false,
	true
};

typedef enum bool bool;
typedef char * string;
#define CHAR_SIZE 1000

#include <string.h>
#include "h/config.h"
#include "h/apache.h"
#include "h/files.h"

void install();
bool exec_cmd(string cmd);
void show_widget(string name);
void gui_main(int *argc, char ***argv);

#endif