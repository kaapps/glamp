#ifndef _GLAMP_FILES_H
#define _GLAMP_FILES_H

struct s_files_list {
	int count;
	char files[CHAR_SIZE][CHAR_SIZE];
};

string files_get_home_folder();
void files_create_folder_if_not_exists(string folder_name);
bool files_write(string file_name, string txt);
struct s_files_list files_list(string folder_name);
bool file_delete(string name);
bool file_delete_lines(string str, string file_name);
string files_read(string file_name);

#endif

