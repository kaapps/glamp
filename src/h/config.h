#ifndef _GLAMP_CONFIG_H
#define _GLAMP_CONFIG_H

string config_user_config_folder();
string config_folder();
string config_files(string file_name);
void config_check_dirs();

#endif