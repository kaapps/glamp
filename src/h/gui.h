#ifndef _GLAMP_GUI_H
#define _GLAMP_GUI_H

#include <gtk/gtk.h>

void gui_delete_host(GtkTreeView *tree);
void gui_get_hosts_list(GtkListStore *list);
void gui_show_widget(string name);
void gui_hide_widget(string name);
void gui_destroy(GtkWidget *widget, gpointer data);
void gui_init_folder(GtkFileChooser *chooser);
void gui_main(int *argc, char ***argv);
GtkWidget * gui_get_widget(string name);
void gui_edit_host(GtkTreeView *tree);

#endif
