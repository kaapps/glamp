#ifndef _GLAMP_APACHE_H
#define _GLAMP_APACHE_H

void apache_restart();
void apache_log_file();
bool apache_activate_host(string host_name);
bool apache_deactivate_host(string host_name);
void apache_delete_host(string host_name);
void apache_create_host(string host_path);
void apache_edit_host(string host_name);

#endif
