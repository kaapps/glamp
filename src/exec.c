#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include "h/header.h"
#include "h/gui.h"

bool exec_cmd(string cmd) {
	int res=system(cmd);	
	if(res!=0) {
		gui_show_widget("error");
	}
	
	return true;
}
