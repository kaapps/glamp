# Glamp #
GUI for LAMP server

## Usage ##

![glamp.png](https://bitbucket.org/repo/7GLLBG/images/2465928890-glamp.png)

### Button Install LAMP server ###
Installs Apache, PHP, MySQL and phpMyAdmin

*It may take a few minutes*

### Button Restart Apache ###
Restarts Apache

### Button Add ###
Adding virtual host

### Button Apache Log File ###
Shows Apache log file

### Button Hosts list ###
Shows virtual hosts

![glamp_list.png](https://bitbucket.org/repo/7GLLBG/images/165453568-glamp_list.png)

### Button Edit ###
Edit virtual host config file

### Button Delete ###
Delete virtual host

### Button Back ###
Back to main window

## phpMyAdmin ##
For phpMyAdmin go to http://yourhost/_pma
## Installation  ##

For **Debian** and **Ubuntu**, [download](/kadirov/glamp/downloads) the last version of the **deb** file and run the following command

```
#!bash

sudo dpkg -i path/to/downloaded/file.deb
```

For other Linux distributions [download repository](/kadirov/glamp/downloads), unzip and run the following commands

```
#!bash

./configure CC=c99
make
sudo make install
```

## License ##
GNU General Public License [www.gnu.org/licenses](http://www.gnu.org/licenses/)